//
//  UIImage+Utilities.swift
//  Movies
//
//  Created by Saurabh Garg on 21/09/2018.
//  Copyright © 2018 Gargs.com. All rights reserved.
//

import UIKit

extension UIImage {
    func resize(to size: CGSize) -> UIImage? {
        UIGraphicsBeginImageContextWithOptions(size, true, 0.0)
        draw(in: CGRect(x: 0.0, y: 0.0, width: size.width, height: size.height))
        let image = UIGraphicsGetImageFromCurrentImageContext()
        return image
    }
}
