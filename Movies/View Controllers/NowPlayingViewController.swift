//
//  NowPlayingViewController.swift
//  Movies
//
//  Created by Saurabh Garg on 12/09/2018.
//  Copyright © 2018 Gargs.com. All rights reserved.
//

import UIKit

class NowPlayingViewController: UICollectionViewController, UICollectionViewDelegateFlowLayout, UISearchControllerDelegate, UISearchResultsUpdating, UICollectionViewDataSourcePrefetching {
    private weak var notificationObserver: NSObjectProtocol?
    
    required init(with currentViewMode: CurrentViewMode) {
        super.init(collectionViewLayout: NowPlayingViewControllerLayout())
        switch currentViewMode {
        case .nowPlaying:
            notificationObserver =  NotificationCenter.default.addObserver(forName: .nowPlayingInitialized, object: nil, queue: OperationQueue.main) { [weak self] (notification) in
                self?.collectionView.scrollRectToVisible(.zero, animated: true)
                self?.collectionView.reloadData()
            }
        default:
            notificationObserver = NotificationCenter.default.addObserver(forName: .searchResultsInitialized, object: nil, queue: OperationQueue.main) { [weak self] (notification) in
                self?.collectionView.scrollRectToVisible(.zero, animated: true)
                self?.collectionView.reloadData()
            }
        }
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    deinit {
        if let observer = notificationObserver {
            NotificationCenter.default.removeObserver(observer)
        }
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        initializeSearchController()
        
        collectionView.prefetchDataSource = self
        setItemSize()
        
        navigationController?.navigationBar.prefersLargeTitles = true
        title = "Movies"
        DataManager.shared.loadNextPage()
        collectionView.register(MovieCollectionViewCell.self, forCellWithReuseIdentifier: MovieCollectionViewCell.ReuseIdentifier)
    }
    
    
    override func traitCollectionDidChange(_ previousTraitCollection: UITraitCollection?) {
        super.traitCollectionDidChange(previousTraitCollection)
        setItemSize()
        collectionView.reloadData()
    }
    
    override func numberOfSections(in collectionView: UICollectionView) -> Int {
        return 1
    }
    
    override func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return DataManager.shared.totalMovies
    }
    
    override func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: MovieCollectionViewCell.ReuseIdentifier, for: indexPath) as! MovieCollectionViewCell
        cell.dataSourceIndex = indexPath.row
        return cell
    }
    
    override func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        let isSearchResult = presentingViewController != nil
        
        guard let movie = DataManager.shared.movies?[indexPath.row] else { return }
        let viewController = MovieDetailsViewController(with: movie)
        if isSearchResult {
            presentingViewController?.navigationController?.pushViewController(viewController, animated: true)
        } else {
            navigationController?.pushViewController(viewController, animated: true)
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, prefetchItemsAt indexPaths: [IndexPath]) {
        // Only prefetch if not already loaded
        guard let minIndexPath = indexPaths.sorted().first, let results = DataManager.shared.movies else { return }
        if minIndexPath.row >= results.endIndex {
            DataManager.shared.loadNextPage()
        }
    }
}

extension NowPlayingViewController {
    func updateSearchResults(for searchController: UISearchController) {
        guard let movieName = searchController.searchBar.text, !movieName.isEmpty else { return }
        DataManager.shared.searchTerm = searchController.searchBar.text
    }
    
    func didDismissSearchController(_ searchController: UISearchController) {
        DataManager.shared.currentViewMode = .nowPlaying
        collectionView.reloadData()
    }
}

extension NowPlayingViewController {
    private func initializeSearchController() {
        guard navigationController?.topViewController == self else { return }
        let viewController = NowPlayingViewController(with: .search)
        let searchController = UISearchController(searchResultsController: viewController)
        searchController.delegate = self
        searchController.searchResultsUpdater = self
        searchController.obscuresBackgroundDuringPresentation = true
        navigationItem.searchController = searchController
        navigationItem.hidesSearchBarWhenScrolling = false
        definesPresentationContext = true
    }
    
    private func setItemSize() {
        let ratio: CGFloat = 1.6
        let layout = collectionViewLayout as! NowPlayingViewControllerLayout
        let width = layout.collectionViewContentSize.width - collectionView.safeAreaInsets.left - collectionView.safeAreaInsets.right
        switch traitCollection.verticalSizeClass {
        case .compact:
            // landscape; show 5 movies per row
            let itemWidth = width / 5
            let itemHeight = itemWidth * ratio
            layout.itemSize = CGSize(width: itemWidth, height: itemHeight)
        default:
            // 2 items per row
            let itemWidth = width / 2
            let itemHeight = itemWidth * ratio
            layout.itemSize = CGSize(width: itemWidth, height: itemHeight)
        }
    }
}
