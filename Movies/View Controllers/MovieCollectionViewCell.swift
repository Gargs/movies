//
//  MovieCollectionViewCell.swift
//  Movies
//
//  Created by Saurabh Garg on 12/09/2018.
//  Copyright © 2018 Gargs.com. All rights reserved.
//

import os.log
import UIKit

fileprivate let Logger = OSLog(subsystem: "com.cerebrawl.Movies", category: "MovieCell")

class MovieCollectionViewCell: UICollectionViewCell {
    static let ReuseIdentifier = "MovieCell"
    private weak var dataTask: URLSessionDataTask?
    private weak var notificationObserver: NSObjectProtocol?
    
    // Probably not the most elegant design as now the cell is coupled with the model, but this is a quick hack
    var dataSourceIndex: Int? {
        didSet {
            setMoviePosterURL()
        }
    }
    
    deinit {
        if let observer = notificationObserver {
            NotificationCenter.default.removeObserver(observer)
        }
    }
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        notificationObserver = NotificationCenter.default.addObserver(forName: .nowPlayingUpdated, object: nil, queue: OperationQueue.main, using: { [weak self] (notification) in
            guard let self = self else { return }
            self.setMoviePosterURL()
        })
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    var posterURL: URL? {
        didSet {
            downloadPoster()
        }
    }
    
    private lazy var posterView: UIImageView = {
        let view = UIImageView(frame: contentView.bounds)
        view.alpha = 0.0
        view.translatesAutoresizingMaskIntoConstraints = false
        contentView.addSubview(view)
        view.topAnchor.constraint(equalTo: contentView.topAnchor).isActive = true
        view.bottomAnchor.constraint(equalTo: contentView.bottomAnchor).isActive = true
        view.leadingAnchor.constraint(equalTo: contentView.leadingAnchor).isActive = true
        view.trailingAnchor.constraint(equalTo: contentView.trailingAnchor).isActive = true
        return view
    }()
    
    override func prepareForReuse() {
        super.prepareForReuse()
        dataTask?.cancel()
        posterView.image = nil
        posterView.alpha = 0.0
    }
}

extension MovieCollectionViewCell {
    private func downloadPoster() {
        let size = posterView.bounds.size
        guard let url = posterURL else { return }
        let session = URLSession.shared
        os_log("Downloading poster from: %@", log: Logger, type: .debug, url.absoluteString)
        let request = URLRequest(url: url)
        OperationQueue().addOperation { [weak self] in
            if let cachedResponse = session.configuration.urlCache?.cachedResponse(for: request) {
                guard let self = self else { return }
                if let image = UIImage(data: cachedResponse.data, scale: UIScreen.main.scale)?.resize(to: size) {
                    OperationQueue.main.addOperation {
                        self.posterView.alpha = 1.0
                        self.posterView.image = image
                    }
                }
            } else {
                self?.dataTask = session.dataTask(with: url, completionHandler: { (data, response, error) in
                    guard
                        let self = self,
                        let data = data,
                        let image = UIImage(data: data, scale: UIScreen.main.scale)?.resize(to: size)
                        else { return }
                    OperationQueue.main.addOperation {
                        self.posterView.image = image
                        UIView.animate(withDuration: 0.3, animations: {
                            self.posterView.alpha = 1.0
                        }, completion: nil)
                    }
                })
                self?.dataTask?.resume()
            }
        }
    }
    
    private func setMoviePosterURL() {
        guard
            let index = self.dataSourceIndex,
            let results = DataManager.shared.movies,
            results.endIndex > index else { return }
        self.posterURL = results[index].posterURL()
    }
}
