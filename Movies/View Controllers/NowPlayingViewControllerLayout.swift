//
//  NowPlayingViewControllerLayout.swift
//  Movies
//
//  Created by Saurabh Garg on 14/09/2018.
//  Copyright © 2018 Gargs.com. All rights reserved.
//

import UIKit

class NowPlayingViewControllerLayout: UICollectionViewFlowLayout {
    override init() {
        super.init()
        itemSize = CGSize(width: 100, height: 150)
        sectionInsetReference = .fromSafeArea
        scrollDirection = .vertical
        minimumLineSpacing = 0.0
        minimumInteritemSpacing = 0.0
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}
