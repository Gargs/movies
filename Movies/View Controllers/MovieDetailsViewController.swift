//
//  MovieDetailsViewController.swift
//  Movies
//
//  Created by Saurabh Garg on 16/09/2018.
//  Copyright © 2018 Gargs.com. All rights reserved.
//

import os.log
import UIKit

fileprivate let Logger = OSLog(subsystem: "com.cerebrawl.Movies", category: "MovieDetail")

class MovieDetailsViewController: UIViewController {
    private weak var scrollView: UIScrollView!
    private weak var contentView: UIView!
    private weak var titleLabel: UILabel!
    private weak var plotLabel: UILabel!
    private weak var backDropImageView: UIImageView!
    private var movie: Movie?
    
    init(with movie: Movie) {
        super.init(nibName: nil, bundle: nil)
        self.movie = movie
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        view.backgroundColor = UIColor.black
        
        downloadBackdrop()
        
        setupScrollView()
        setupContentView()
        setupBackDrop()
        setupTextViews()
        
        titleLabel.text = movie?.title
        plotLabel.text = movie?.plotOverview
    }
}

extension MovieDetailsViewController {
    private func downloadBackdrop() {
        guard let url = movie?.backDropURL() else { return }
        let session = URLSession.shared
        os_log("Download backdrop from: %@", log: Logger, type: .debug, url.absoluteString)
        let dataTask = session.dataTask(with: url, completionHandler: { [weak self] (data, response, error) in
            guard
                let self = self,
                let data = data,
                let image = UIImage(data: data, scale: UIScreen.main.scale)
                else { return }
            OperationQueue.main.addOperation {
                self.backDropImageView.image = image
            }
        })
        dataTask.resume()
    }
}

extension MovieDetailsViewController {
    private func setupScrollView() {
        let scrollView = UIScrollView(frame: view.bounds)
        scrollView.translatesAutoresizingMaskIntoConstraints = false
        view.addSubview(scrollView)
        let safeAreaLayoutGuide = view.safeAreaLayoutGuide
        scrollView.topAnchor.constraint(equalTo: safeAreaLayoutGuide.topAnchor).isActive = true
        scrollView.bottomAnchor.constraint(equalTo: safeAreaLayoutGuide.bottomAnchor).isActive = true
        scrollView.leadingAnchor.constraint(equalTo: safeAreaLayoutGuide.leadingAnchor).isActive = true
        scrollView.trailingAnchor.constraint(equalTo: safeAreaLayoutGuide.trailingAnchor).isActive = true
        self.scrollView = scrollView
    }
    
    private func setupContentView() {
        let view = UIView()
        view.translatesAutoresizingMaskIntoConstraints = false
        scrollView.addSubview(view)
        view.topAnchor.constraint(equalTo: scrollView.topAnchor).isActive = true
        view.bottomAnchor.constraint(equalTo: scrollView.bottomAnchor).isActive = true
        view.leadingAnchor.constraint(equalTo: scrollView.leadingAnchor).isActive = true
        view.trailingAnchor.constraint(equalTo: scrollView.trailingAnchor).isActive = true
        view.widthAnchor.constraint(equalTo: scrollView.widthAnchor).isActive = true
        self.contentView = view
    }
    
    private func setupBackDrop() {
        let imageView = UIImageView()
        imageView.translatesAutoresizingMaskIntoConstraints = false
        contentView.addSubview(imageView)
        imageView.topAnchor.constraint(equalTo: contentView.topAnchor).isActive = true
        imageView.leadingAnchor.constraint(equalTo: contentView.leadingAnchor).isActive = true
        imageView.trailingAnchor.constraint(equalTo: contentView.trailingAnchor).isActive = true
        imageView.heightAnchor.constraint(equalTo: contentView.widthAnchor, multiplier: 0.625).isActive = true
        self.backDropImageView = imageView
    }
    
    private func setupTextViews() {
        let titleLabel = UILabel()
        titleLabel.numberOfLines = 0
        titleLabel.lineBreakMode = .byWordWrapping
        titleLabel.textColor = UIColor.white
        titleLabel.font = UIFont.preferredFont(forTextStyle: .largeTitle)
        titleLabel.translatesAutoresizingMaskIntoConstraints = false
        contentView.addSubview(titleLabel)
        let layoutGuide = contentView.readableContentGuide
        titleLabel.leadingAnchor.constraint(equalTo: layoutGuide.leadingAnchor).isActive = true
        titleLabel.trailingAnchor.constraint(equalTo: layoutGuide.trailingAnchor).isActive = true
        titleLabel.topAnchor.constraint(equalTo: backDropImageView.bottomAnchor, constant: 20.0).isActive = true
        self.titleLabel = titleLabel
        
        let plotLabel = UILabel()
        plotLabel.textColor = UIColor.white
        plotLabel.numberOfLines = 0
        plotLabel.lineBreakMode = .byWordWrapping
        plotLabel.font = UIFont.preferredFont(forTextStyle: .subheadline)
        plotLabel.translatesAutoresizingMaskIntoConstraints = false
        contentView.addSubview(plotLabel)
        plotLabel.leadingAnchor.constraint(equalTo: layoutGuide.leadingAnchor).isActive = true
        plotLabel.trailingAnchor.constraint(equalTo: layoutGuide.trailingAnchor).isActive = true
        plotLabel.topAnchor.constraint(equalTo: titleLabel.bottomAnchor, constant: 30.0).isActive = true
        contentView.bottomAnchor.constraint(equalTo: plotLabel.bottomAnchor).isActive = true
        self.plotLabel = plotLabel
    }
}
