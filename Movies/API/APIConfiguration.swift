//
//  APIConfiguration.swift
//  Movies
//
//  Created by Saurabh Garg on 11/09/2018.
//  Copyright © 2018 Gargs.com. All rights reserved.
//

import Foundation

extension Notification.Name {
    static let apiConfigurationLoaded = Notification.Name("apiConfigurationLoaded")
}

struct APIConfiguration: Decodable {
    static let PosterSize = "w500"
    static let backDropSize = "w780"
    fileprivate let imageConfiguration: ImageConfiguration
    var imageBaseURL: URL {
        return APIQueue.sync {
            return URL(string: imageConfiguration.baseURLString)!
        }
    }
    
    private enum CodingKeys: String, CodingKey {
        case imageConfiguration = "images"
    }
}

fileprivate struct ImageConfiguration: Decodable {
    let baseURLString: String
    
    private enum CodingKeys: String, CodingKey {
        case baseURLString = "secure_base_url"
    }
}

extension APIConfiguration {
    static private(set) var shared: APIConfiguration? {
        didSet {
            NotificationCenter.default.post(name: .apiConfigurationLoaded, object: shared)
        }
    }
    
    static func load() {
        downloadConfiguration { (configuration, error) in
            APIConfiguration.shared = configuration
        }
    }
}
