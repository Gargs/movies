//
//  APIHelper.swift
//  Movies
//
//  Created by Saurabh Garg on 11/09/2018.
//  Copyright © 2018 Gargs.com. All rights reserved.
//

import Foundation

private let scheme = "https"
private let hostName = "api.themoviedb.org"
private let apiVersion = "3"
private let apiKey = "99a9a789929dc58e95b4cd7d0583cab7"

let APIQueue = DispatchQueue(label: "com.cerebrawl.Movies.api.Dispatch", qos: .background)
let APIOperationQueue: OperationQueue = {
    let queue = OperationQueue()
    queue.underlyingQueue = APIQueue
    queue.name = "com.cerebrawl.Movies.api.Operation"
    return queue
}()

private let session = URLSession(configuration: URLSessionConfiguration.default, delegate: nil, delegateQueue: APIOperationQueue)
private weak var nowPlayingDataTask: URLSessionDataTask?
private weak var searchDataTask: URLSessionDataTask?

private enum MoviesAPI {
    case configuration
    case nowPlaying(pageNumber: Int?)
    case search(query: String, pageNumber: Int?)
    case movie(movieId: Int)
    
    func url() -> URL? {
        var urlComponents = URLComponents()
        urlComponents.scheme = scheme
        urlComponents.host = hostName
        
        var queryItems = [URLQueryItem]()
        
        queryItems.append(URLQueryItem(name: "api_key", value: apiKey))
        
        switch self {
        case .configuration:
            urlComponents.path = "/" + apiVersion + "/configuration"
        case .nowPlaying(let pageNumber):
            urlComponents.path = "/" + apiVersion + "/movie/now_playing"
            if let pageNumber = pageNumber {
                queryItems.append(URLQueryItem(name: "page", value: String(pageNumber)))
            }
        case .search(let query, let pageNumber):
            urlComponents.path = "/" + apiVersion + "/search/movie"
            queryItems.append(URLQueryItem(name: "query", value: query))
            if let pageNumber = pageNumber {
                queryItems.append(URLQueryItem(name: "page", value: String(pageNumber)))
            }
        case .movie(let movieId):
            urlComponents.path = "/" + apiVersion + "/movie/" + String(movieId)
        }
        
        urlComponents.queryItems = queryItems
        return urlComponents.url
    }
}

func downloadConfiguration(completionHandler: @escaping (APIConfiguration?, Error?) -> ()) {
    APIOperationQueue.addOperation {
        guard let url = MoviesAPI.configuration.url() else { return }
        let request = URLRequest(url: url)
        let downloadTask = session.dataTask(with: request) { (data, response, error) in
            var configuration: APIConfiguration? = nil
            if let data = data {
                configuration = try? JSONDecoder().decode(APIConfiguration.self, from: data)
            }
            completionHandler(configuration, error)
        }
        downloadTask.resume()
    }
}

func downloadNowPlaying(pageNumber page: Int = 1, completionHandler: @escaping (NowPlaying?, Error?) -> ()) {
    guard nowPlayingDataTask == nil else { return } // make sure that only one page is being downloaded at a time
    APIOperationQueue.addOperation {
        guard let url = MoviesAPI.nowPlaying(pageNumber: page).url() else { return }
        let request = URLRequest(url: url)
        nowPlayingDataTask = session.dataTask(with: request) { (data, response, error) in
            var nowPlaying: NowPlaying? = nil
            if let data = data {
                nowPlaying = try? JSONDecoder().decode(NowPlaying.self, from: data)
            }
            completionHandler(nowPlaying, error)
        }
        nowPlayingDataTask!.resume()
    }
}

func downloadMovieDetails(movieID id: Int, completionHandler: @escaping (Movie?, Error?) -> ()) {
    APIOperationQueue.addOperation {
        guard let url = MoviesAPI.movie(movieId: id).url() else { return }
        let request = URLRequest(url: url)
        let downloadTask = session.dataTask(with: request) { (data, response, error) in
            var movie: Movie? = nil
            if let data = data {
                movie = try? JSONDecoder().decode(Movie.self, from: data)
            }
            completionHandler(movie, error)
        }
        downloadTask.resume()
    }
}

func searchMovies(name: String, pageNumber page: Int = 1, completionHandler: @escaping (SearchResult?, Error?) -> ()) {
    guard searchDataTask == nil else { return }
    APIOperationQueue.addOperation {
        guard let url = MoviesAPI.search(query: name, pageNumber: page).url() else { return }
        let request = URLRequest(url: url)
        searchDataTask = session.dataTask(with: request) { (data, response, error) in
            var searchResult: SearchResult? = nil
            if let data = data {
                searchResult = try? JSONDecoder().decode(SearchResult.self, from: data)
            }
            completionHandler(searchResult, error)
        }
        searchDataTask!.resume()
    }
}
