//
//  Movie.swift
//  Movies
//
//  Created by Saurabh Garg on 11/09/2018.
//  Copyright © 2018 Gargs.com. All rights reserved.
//

import Foundation

struct Movie: Decodable {
    let id: Int
    let title: String
    let backdrop: String?
    let poster: String?
    let plotOverview: String?
    
    private enum CodingKeys: String, CodingKey {
        case id
        case title
        case backdrop = "backdrop_path"
        case poster = "poster_path"
        case plotOverview = "overview"
    }
}
    
extension Movie {
    func backDropURL() -> URL? {
        guard var baseURL = APIConfiguration.shared?.imageBaseURL, let backdrop = backdrop else { return nil }
        baseURL.appendPathComponent(APIConfiguration.backDropSize)
        baseURL.appendPathComponent(backdrop)
        return baseURL
    }
    
    func posterURL() -> URL? {
        guard var baseURL = APIConfiguration.shared?.imageBaseURL, let poster = poster else { return nil }
        baseURL.appendPathComponent(APIConfiguration.PosterSize)
        baseURL.appendPathComponent(poster)
        return baseURL
    }
}
