//
//  SearchResult.swift
//  Movies
//
//  Created by Saurabh Garg on 11/09/2018.
//  Copyright © 2018 Gargs.com. All rights reserved.
//

import Foundation

struct SearchResult: Decodable {
    let results: [Movie]
    let totalPages: Int
    let currentPage: Int
    let totalResults: Int
    
    private enum CodingKeys: String, CodingKey {
        case results
        case totalPages = "total_pages"
        case currentPage = "page"
        case totalResults = "total_results"
    }
}

typealias NowPlaying = SearchResult
