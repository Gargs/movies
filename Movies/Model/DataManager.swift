//
//  DataManager.swift
//  Movies
//
//  Created by Saurabh Garg on 12/09/2018.
//  Copyright © 2018 Gargs.com. All rights reserved.
//

import Foundation

extension Notification.Name {
    static let nowPlayingInitialized = Notification.Name("nowPlayingInitialized") // used to reload collectionview
    static let nowPlayingUpdated = Notification.Name("nowPlayingUpdated") // used to update cells
    static let searchResultsInitialized = Notification.Name("searchResultsInitialized") // used to reload collectionview
    static let searchResultsUpdated = Notification.Name("searchResultsUpdated") // used to update cells
}

enum CurrentViewMode {
    case nowPlaying
    case search
}

class DataManager {
    static let shared = DataManager()
    
    private init() {
        initialLoad()
    }
    
    var currentViewMode: CurrentViewMode = .nowPlaying {
        didSet {
            if currentViewMode == .nowPlaying {
                searchResult = nil
            }
        }
    }
    
    var movies: [Movie]? {
        get {
            switch currentViewMode {
            case .nowPlaying:
                return nowPlaying?.results
            case .search:
                return searchResult?.results
            }
        }
    }
    
    var totalMovies: Int {
        get {
            switch currentViewMode {
            case .nowPlaying:
                return nowPlaying?.totalResults ?? 0
            case .search:
                return searchResult?.totalResults ?? 0
            }
        }
    }
    
    private(set) var nowPlaying: NowPlaying? {
        didSet {
            if oldValue == nil || nowPlaying == nil {
                NotificationCenter.default.post(name: .nowPlayingInitialized, object: self)
            }
        }
    }
    private(set) var searchResult: SearchResult? {
        didSet {
            if oldValue == nil || searchResult == nil {
                NotificationCenter.default.post(name: .searchResultsInitialized, object: self)
            }
        }
    }
    var searchTerm: String? {
        didSet {
            if searchTerm != oldValue { searchResult = nil }
            guard let searchTerm = searchTerm else { return }
            currentViewMode = .search
            searchMovies(name: searchTerm) { (list, error) in
                guard let list = list else { return }
                OperationQueue.main.addOperation {
                    self.searchResult = list
                }
            }
        }
    }
}

extension DataManager {
    private func initialLoad() {
        downloadNowPlaying { [unowned self] (list, error) in
            guard let list = list else { return }
            OperationQueue.main.addOperation {
                self.nowPlaying = list
            }
        }
    }
    
    func loadNextPage() {
        switch currentViewMode {
        case .nowPlaying:
            guard let nowPlaying = nowPlaying else { return }
            downloadNowPlaying(pageNumber: nowPlaying.currentPage + 1) { (list, error) in
                if let list = list {
                    OperationQueue.main.addOperation {
                        self.nowPlaying = NowPlaying(results: nowPlaying.results + list.results, totalPages: list.totalPages, currentPage: list.currentPage, totalResults: list.totalResults)
                        NotificationCenter.default.post(name: .nowPlayingUpdated, object: self)
                    }
                }
            }
        case .search:
            guard let name = searchTerm, let searchResult = searchResult else { return }
            searchMovies(name: name, pageNumber: searchResult.currentPage + 1) { (list, error) in
                if let list = list {
                    OperationQueue.main.addOperation {
                        self.searchResult = SearchResult(results: searchResult.results + list.results, totalPages: list.totalPages, currentPage: list.currentPage, totalResults: list.totalResults)
                        NotificationCenter.default.post(name: .searchResultsUpdated, object: self)
                    }
                }
            }
        }
    }
}
